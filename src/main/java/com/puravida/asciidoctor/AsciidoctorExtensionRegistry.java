package com.puravida.asciidoctor;

import com.puravida.asciidoctor.calendar.CalendarBlock;
import com.puravida.asciidoctor.clipboard.ClipboardPostprocessor;
import com.puravida.asciidoctor.collapsable.CollapsablePostProcessor;
import com.puravida.asciidoctor.disqus.DisqusMacroProcessor;
import com.puravida.asciidoctor.eachfile.EachFilePreprocessor;
import com.puravida.asciidoctor.google.GoogleAnalyticsDocinfoProcessor;
import com.puravida.asciidoctor.google.GoogleSearchDocinfoProcessor;
import com.puravida.asciidoctor.https.HttpsPostProcessor;
import com.puravida.asciidoctor.multilanguage.MultiLanguageMacroProcessor;
import com.puravida.asciidoctor.multilanguage.MultiLanguagePostProcessor;
import com.puravida.asciidoctor.tooltip.TooltipPostprocessor;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jorge on 18/06/17.
 */
public class AsciidoctorExtensionRegistry implements ExtensionRegistry {

    @Override
    public void register(Asciidoctor asciidoctor) {

        {
            Map<String, Object> options = new HashMap<String, Object>();
            options.put("location", ":footer");
            GoogleAnalyticsDocinfoProcessor googleAnalyticsDocinfo = new GoogleAnalyticsDocinfoProcessor(options);
            asciidoctor.javaExtensionRegistry().docinfoProcessor(googleAnalyticsDocinfo);
        }

        asciidoctor.javaExtensionRegistry().blockMacro(DisqusMacroProcessor.TAG, DisqusMacroProcessor.class);

        asciidoctor.javaExtensionRegistry().postprocessor(CollapsablePostProcessor.class);

        asciidoctor.javaExtensionRegistry().postprocessor(ClipboardPostprocessor.class);

        asciidoctor.javaExtensionRegistry().docinfoProcessor(GoogleSearchDocinfoProcessor.class);

        asciidoctor.javaExtensionRegistry().postprocessor(TooltipPostprocessor.class);

        asciidoctor.javaExtensionRegistry().postprocessor(MultiLanguagePostProcessor.class);

        asciidoctor.javaExtensionRegistry().blockMacro(MultiLanguagePostProcessor.TAG, MultiLanguageMacroProcessor.class);

        //asciidoctor.javaExtensionRegistry().treeprocessor(MultiLanguageTreeProcessor.class);

        asciidoctor.javaExtensionRegistry().postprocessor(HttpsPostProcessor.class);

        asciidoctor.javaExtensionRegistry().preprocessor(EachFilePreprocessor.class);

        asciidoctor.javaExtensionRegistry().block("calendar", CalendarBlock.class);
    }

}
