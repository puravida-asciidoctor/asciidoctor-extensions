$(document).ready(function() {
    $('.listingblock').each( function() {
        var listingblock = $(this);
        var colist= listingblock.next();
        if( !colist.hasClass('colist') ) return;
        var texts = colist.find('tr');
        $('.conum', listingblock).each(function(index){
            var conum = $(this);
            var txt = texts.length > index ? $(texts[index]).find('td:last').html() : '&nbsp;';
            conum.addClass('tooltip').append('<span class="tooltiptext">'+txt+'</span>')
        });
    });
});
