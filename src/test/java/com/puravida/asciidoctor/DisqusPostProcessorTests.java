package com.puravida.asciidoctor;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jorge on 10/06/17.
 */
public class DisqusPostProcessorTests {
    @Test
    public void postProcessorIsApplied() {
        Map<String,Object> attributes = new HashMap<String,Object>();
        attributes.put("disqus","asciidoctor-extensions");

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n\n" +
                        "disqus::asciidoctor-extensions[] \n"+
                        ""), options);
        assertThat(converted).contains("'https://asciidoctor-extensions.disqus.com/embed.js'");
    }

}
