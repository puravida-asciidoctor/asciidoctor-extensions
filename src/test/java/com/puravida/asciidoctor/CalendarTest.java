package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CalendarTest {

    @Test
    public void simpleCalendar() throws Exception{
        Map<String,Object> attributes = new HashMap<String,Object>();

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        ":icons: font\n\n"+
                        ".Vacaciones 2019\n"+
                        "[calendar,2019,1] \n"+
                        "---- \n"+
                        "11 icon:bus[] salida Madrid 12:00\n"+
                        "11 icon:bus[] llegada Barcelona 18:00\n"+
                        "12 icon:plane[] salida Barcelona 14:30\n"+
                        "15 icon:anchor[] Scuba\n"+
                        "17 icon:binoculars[] Bird Watching\n"+
                        "23 icon:car[] llegada Madrid 19:00\n"+
                        "---- \n"+
                        ""), options);

        PrintWriter fos = new PrintWriter(new FileOutputStream("build/calendartest.html"));
        fos.println(converted);

        assert converted.indexOf("2019") !=-1;
    }

}
