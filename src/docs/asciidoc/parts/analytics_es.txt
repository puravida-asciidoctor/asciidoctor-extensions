
[role='language-es']
== Google Analytics

Simplemente usando el atributo *google-analytics-code* en la cabecera de tu documento, la extensión
añadirá el código javascript necesario al final del documento HTML usando el código de Google Analytics
que hayas especificado

Por ejemplo

[#ganalitics,source]
----
= My document
Author
:doctype: book
:google-analytics-code: UA-000000-00
----

Generará

....
<!-- google -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-000000-00', 'auto');
ga('send', 'pageview');
</script>
....

